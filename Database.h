#pragma once

#include <string>

/**
 * @brief Simple wrapper ower database commands
 */
class Database
{
public:
    Database() {}
    void create(std::string name);
};
