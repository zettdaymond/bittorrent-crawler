#pragma once

#include <exception>
#include <string>

class StrException : public std::exception
{
public:
    StrException(const std::string& what) : m_what(what) {}
    ~StrException() = default;

    const char* what() const throw() override {
        return m_what.c_str();
    }
private:
    std::string m_what;
};
