#pragma once

#include <vector>

#include <libtorrent/session.hpp>
#include <libtorrent/add_torrent_params.hpp>

#include "thirdparty/sqlite3pp/sqlite3pp.h"

#include "Types.h"
#include "Database.h"

namespace sql = sqlite3pp;
namespace lt  = libtorrent;

struct FetcherSettings
{
    uint port = 39950;
    uint queue_size = 20; //torrents
    uint timeout = 60 * 2; //secs;
    uint download_rate_per_torrent = 2048;
    std::string save_path = "./";
    lt::add_torrent_params torrent_add_params;
};

class Fetcher
{
    using TorrentHandle =
        std::tuple<
            lt::torrent_handle,
            std::chrono::time_point<std::chrono::system_clock>
        >;
    using TorrentHandleContainer = std::deque<TorrentHandle>;
    using SessionPtr = std::unique_ptr<libtorrent::session>;
public:
    Fetcher(Database& db);
    Fetcher(Database& db, FetcherSettings s);
    ~Fetcher();
    void load_state(std::string filename);
    void save_state(std::string filename);
    void step();
    void stop_deffered();
    void add_to_dl(const std::vector<std::string>& info_hashes);

    std::vector<TorrentMeta> get_metadata();
    void remove_queued_hashes(std::vector<std::string>& hashes);
private:
    SessionPtr m_session;
    FetcherSettings m_settings;
    TorrentHandleContainer m_torrent_handles;
    Database& m_db;
};
