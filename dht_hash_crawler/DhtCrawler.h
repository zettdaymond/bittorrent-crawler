#pragma once

#include <libtorrent/session.hpp>
#include "Types.h"

#include "Database.h"

namespace lt = libtorrent;
namespace sql = sqlite3pp;

struct CrawlerSettings
{
    // session settings
    int upload_rate_limit = 200000;
    int download_rate_limit = 200000;
    int active_downloads = 50;
    int alert_queue_size = 4000;
    int dht_announce_interval = 60;
    int torrent_upload_limit = 20000;
    int torrent_download_limit = 20000;
    int auto_manage_startup = 30;
    int auto_manage_interval = 15;

    // crawler settings
    int start_port = 32900;
    int session_num = 1;
    int total_intervals = 60;
    int writing_interval = 60;
    int max_download_duration = 75;

    bool use_get_peers_hashes = true;
};

class DhtCrawler
{
    using SessionPtr = std::unique_ptr<libtorrent::session>;
public:
    DhtCrawler(Database& db);
    DhtCrawler(Database& db, CrawlerSettings s);
    ~DhtCrawler();
    void step();
    void save_state(std::string filename);
    void load_state(std::string filename);
private:
    std::vector<Message> get_interesting_dht_messages();
    Message construct_message(libtorrent::alert* palert);
    std::vector<std::string> extract_info_hashes(std::vector<Message> alerts);
    bool is_interesting(libtorrent::alert* a);

    CrawlerSettings m_settings;
    SessionPtr m_session;
    Database& m_db;
};
