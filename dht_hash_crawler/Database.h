#pragma once

#include <string>
#include <vector>
#include <string>

#include "thirdparty/sqlite3pp/sqlite3pp.h"

#include "Types.h"

namespace sql = sqlite3pp;

/**
 * @brief Simple wrapper ower database commands
 */
class Database
{
public:
    enum class DHTMessage {
        ANNOUNCE,
        GET_PEERS
    };

public:
    Database(std::string name);
    ~Database();
    void create_tables();
    void save_info_hashes(std::vector<std::string>& hashes);
    std::vector<std::string> get_not_presented_info_hashes(const std::vector<std::string>& hashes);
    void save_torrent_metadata(std::vector<TorrentMeta>& metadata);
    std::vector<std::string> get_sorted_announce_hashes(uint limit = 10000);
    std::vector<std::string> get_sorted_hashes(DHTMessage hash_aquire_method, uint limit = 100000);
    void save_dht_messages(std::vector<Message> messages);
    std::vector<std::string> get_sorted_hashes(uint limit);
private:
    sql::database db;
};
