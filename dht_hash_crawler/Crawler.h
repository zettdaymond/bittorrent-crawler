#pragma once

#include <string>
#include <vector>
#include <memory>
#include <deque>

namespace libtorrent {
class session;
class alert;
}

#include <libtorrent/session.hpp>

using SessionPtr = std::unique_ptr<libtorrent::session>;

enum class InfoHashType {
    ANNOUNCE,
    GET_PEERS,
};

struct InfoHashAdditionalData
{
    int port;
    boost::asio::ip::address ip;
};

using InfoHash = std::pair<std::string, InfoHashType>;

bool check(InfoHash& h);

struct CrawlerSettings
{
    // session settings
    int upload_rate_limit = 200000;
    int download_rate_limit = 200000;
    int active_downloads = 50;
    int alert_queue_size = 4000;
    int dht_announce_interval = 60;
    int torrent_upload_limit = 20000;
    int torrent_download_limit = 20000;
    int auto_manage_startup = 30;
    int auto_manage_interval = 15;

    // crawler settings
    int start_port = 32900;
    int session_num = 1;
    int total_intervals = 60;
    int writing_interval = 60;
    int max_download_duration = 75;

    std::string result_file;
    std::vector< std::pair<std::string, int> > dht_routers;
};

struct FileMeta
{
    std::string path;
    uint size;
};

struct TorrentMeta
{
    std::string name;
    std::string info_hash;
    std::size_t size;
    std::vector<FileMeta> files;
};


class Crawler
{
public:
    Crawler(CrawlerSettings init);
    void create_sessions();
    std::vector<TorrentMeta> get_metadata();
    void add_to_download_list(std::vector<std::string>& info_hashes);
    std::vector<InfoHash> obtaining_info_hashes();
    std::vector<libtorrent::alert*> get_interesting_dht_messages();
    std::size_t on_downloading();
    void remove_invalid_torrents();
    bool is_interesting(libtorrent::alert* a);
    std::vector<std::string> extract_info_hashes(std::vector<libtorrent::alert*> alerts);
    void remove_expired();
    void remove_with_metadata();
private:
    InfoHash handle_alert(libtorrent::alert* palert);
    void handle_info_hash(std::string& info_hash);

    CrawlerSettings m_settings;
    std::vector<SessionPtr> m_sessions;
//    std::vector<libtorrent::torrent_handle> m_torrent_handles;

    std::deque<
        std::tuple<
            libtorrent::torrent_handle,
            std::chrono::time_point<std::chrono::system_clock>
        >
    > m_torrent_handles;
};
