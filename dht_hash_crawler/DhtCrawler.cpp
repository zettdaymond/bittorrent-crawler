#include "DhtCrawler.h"

#include <string>
#include <fstream>

#include <libtorrent/alert.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/bencode.hpp>
#include <libtorrent/bdecode.hpp>
#include <libtorrent/error_code.hpp>
#include <libtorrent/entry.hpp>

#include "Utils.h"
#include "Exception.h"

DhtCrawler::DhtCrawler(Database& db) : m_db(db)
{
    m_session = SessionPtr(new libtorrent::session());
    m_session->set_alert_mask(libtorrent::alert::category_t::dht_notification);

    m_session->add_dht_router(std::make_pair("router.bittorrent.com", 6881));
    m_session->add_dht_router(std::make_pair("router.utorrent.com", 6881));
    lt::error_code ec;
    m_session->listen_on(std::make_pair(6800,6800), ec);
    //m_session->add_dht_router(std::make_pair("router.bitcomet.com", 6881));
    //m_session->add_dht_router(std::make_pair("dht.transmissionbt.com", 6881));

    m_session->start_dht();

    libtorrent::session_settings settings = m_session->settings();
    settings.upload_rate_limit = m_settings.upload_rate_limit;
    settings.download_rate_limit = m_settings.download_rate_limit;
    settings.active_downloads = m_settings.active_downloads;
    settings.auto_manage_interval = m_settings.auto_manage_interval;
    settings.auto_manage_startup = m_settings.auto_manage_startup;
    settings.dht_announce_interval = m_settings.dht_announce_interval;
    settings.alert_queue_size = m_settings.alert_queue_size;

    m_session->set_settings(settings);
}

DhtCrawler::DhtCrawler(Database& db, CrawlerSettings s) : DhtCrawler(db)
{
    m_settings = s;
}

DhtCrawler::~DhtCrawler()
{
   LOGI("Crawler destroyed");
}

void DhtCrawler::step()
{
    auto messages = get_interesting_dht_messages();

    auto info_hashes = extract_info_hashes(messages);
    auto new_hashes = m_db.get_not_presented_info_hashes(info_hashes);

    auto unique_hashes = join_same_hashes(new_hashes);

    if(not unique_hashes.empty()) {
        LOGI("Save unique info_hashes to db. Total (announce + get_peers) : {}", unique_hashes.size());
        m_db.save_info_hashes(unique_hashes);
    }

    //TODO: if config::need_to_save
    m_db.save_dht_messages(messages);
}

void DhtCrawler::save_state(std::string filename)
{
    lt::entry e;
    m_session->save_state(e);

    std::vector<char> out_buf;
    lt::bencode(std::back_inserter(out_buf), e);

    std::ofstream out_stream(filename, std::ios::trunc);
    out_stream.write(&out_buf[0], out_buf.size());
    out_stream.close();
}

void DhtCrawler::load_state(std::string filename)
{
    std::ifstream in_stream(filename);
    std::vector<char> buf((std::istreambuf_iterator<char>(in_stream)),
                          std::istreambuf_iterator<char>());

    lt::bdecode_node dn;
    lt::error_code ec;
    lt::bdecode(&buf[0], &buf[0] + buf.size(), dn, ec);

    if(ec) {
        throw StrException(ec.message());
    }

    LOGD("Crawler session on disk state loaded : {}", dn.string_ptr());

    m_session->load_state(dn);
}


std::vector<std::string> DhtCrawler::extract_info_hashes(std::vector<Message> messages)
{
    std::vector<std::string> info_hashes;
    for(auto& m : messages) {
        if(not m.info_hash.empty()) {
            info_hashes.push_back(m.info_hash);
        }
    }
    return info_hashes;
}


std::vector<Message> DhtCrawler::get_interesting_dht_messages()
{
    std::vector<Message> out;
    std::vector<libtorrent::alert*> alerts;

    m_session->post_torrent_updates();
    m_session->pop_alerts(&alerts);

    for (auto* a : alerts) {
        if(is_interesting(a)) {
            auto message = construct_message(a);
            out.push_back(std::move(message));
        }
        LOGD("Crawler DHT msg : {}", a->message());
    }

    LOGD("Total alerts spawned: {}. Total interesting extracted: {}", alerts.size(), out.size());

    lt::entry e = m_session->dht_state();
    LOGD("DHT Crawler node count {}", e["nodes"].list().size());

    return out;
}

Message DhtCrawler::construct_message(libtorrent::alert* palert)
{
    Message message;

    switch (palert->type())
    {
    case libtorrent::dht_announce_alert::alert_type:
    {
        auto p2 = static_cast<libtorrent::dht_announce_alert*> (palert);

        //convert to hex digits
        std::stringstream ss;
        ss << p2->info_hash;

        message.info_hash = ss.str();
        message.ip = p2->ip.to_string();
        message.port = p2->port;
        message.type = Message::Type::ANNOUNCE;

        break;
    }
    case libtorrent::dht_get_peers_alert::alert_type:
    {
        auto p3 = (libtorrent::dht_get_peers_alert*) palert;

        //convert to hex digits
        std::stringstream ss;
        ss << p3->info_hash;

        message.info_hash = ss.str();
        message.type = Message::Type::GET_PEERS;
        break;
    }
    default:
        assert(false && "This is impossible. Only 2 types of message handles before.");
        break;
    }

    return message;
}

bool DhtCrawler::is_interesting(libtorrent::alert* a)
{
    return (m_settings.use_get_peers_hashes && a->type() == libtorrent::dht_get_peers_alert::alert_type) ||
            a->type() == libtorrent::dht_announce_alert::alert_type;
}
