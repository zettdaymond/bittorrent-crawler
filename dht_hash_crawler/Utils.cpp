#include "Utils.h"

#include <unordered_set>

void init_log(Settings::LOG_LEVEL level) {
    auto log = spdlog::stdout_logger_st("default", true);

    switch (level)
    {
    case Settings::LOG_LEVEL::DUBUG:
        spdlog::set_level(spdlog::level::debug);
        break;
    case Settings::LOG_LEVEL::ERROR:
        spdlog::set_level(spdlog::level::err);
        break;
    case Settings::LOG_LEVEL::INFO:
        spdlog::set_level(spdlog::level::info);
        break;
    case Settings::LOG_LEVEL::WARNING:
        spdlog::set_level(spdlog::level::warn);
        break;
    default:
        assert(false && "Unreachable branch");
    }
}

void init_log(std::string path, Settings::LOG_LEVEL level) {
    spdlog::basic_logger_st("default", path);
    switch (level)
    {
    case Settings::LOG_LEVEL::DUBUG:
        spdlog::set_level(spdlog::level::debug);
        break;
    case Settings::LOG_LEVEL::ERROR:
        spdlog::set_level(spdlog::level::err);
        break;
    case Settings::LOG_LEVEL::INFO:
        spdlog::set_level(spdlog::level::info);
        break;
    case Settings::LOG_LEVEL::WARNING:
        spdlog::set_level(spdlog::level::warn);
        break;
    default:
        assert(false && "Unreachable branch");
    }
}

std::vector<std::string> join_same_hashes(const std::vector<std::string>& hashes)
{
    std::unordered_set<std::string> tmp_set;

    for(auto& h : hashes) {
//        if()
        tmp_set.insert(h);
    }

    std::vector<std::string> output(tmp_set.size());
    std::copy(tmp_set.begin(), tmp_set.end(), output.begin());

    return output;
}

bool replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}
