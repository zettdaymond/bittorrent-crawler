#include "Crawler.h"

#include <fstream>
#include <iostream>
#include <unistd.h>

#include <libtorrent/error_code.hpp>
#include <libtorrent/session.hpp>
#include <libtorrent/session_handle.hpp>
#include <libtorrent/entry.hpp>
#include <libtorrent/bencode.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/alert.hpp>
#include <libtorrent/settings_pack.hpp>
#include <libtorrent/session_settings.hpp>
#include <libtorrent/magnet_uri.hpp>
#include "libtorrent/torrent_info.hpp"
#include <libtorrent/torrent_handle.hpp>

#include <libtorrent/extensions/metadata_transfer.hpp>
#include <libtorrent/extensions/ut_metadata.hpp>
#include <libtorrent/extensions/ut_pex.hpp>


#include "Utils.h"

Crawler::Crawler(CrawlerSettings init) : m_settings(init)
{
    //Load default trackers.
    if(m_settings.dht_routers.size() == 0) {
        m_settings.dht_routers.push_back(std::make_pair("router.bittorrent.com", 6881));
        m_settings.dht_routers.push_back(std::make_pair("router.utorrent.com", 6881));
        m_settings.dht_routers.push_back(std::make_pair("router.bitcomet.com", 6881));
        m_settings.dht_routers.push_back(std::make_pair("dht.transmissionbt.com", 6881));
    }
}

void Crawler::create_sessions()
{
    int& start_port = m_settings.start_port;

    for (int i = 0; i < m_settings.session_num; ++i)
    {
        auto psession = new libtorrent::session();
        psession->set_alert_mask(libtorrent::alert::category_t::all_categories);
        libtorrent::error_code ec;
        psession->listen_on(std::make_pair(start_port + i, start_port + i),ec);

        for (int j = 0; j < m_settings.dht_routers.size(); ++j) {
            psession->add_dht_router(m_settings.dht_routers[j]);
        }

        libtorrent::session_settings settings = psession->settings();
        settings.upload_rate_limit = m_settings.upload_rate_limit;
        settings.download_rate_limit = m_settings.download_rate_limit;
        settings.active_downloads = m_settings.active_downloads;
        settings.auto_manage_interval = m_settings.auto_manage_interval;
        settings.auto_manage_startup = m_settings.auto_manage_startup;
        settings.dht_announce_interval = m_settings.dht_announce_interval;
        settings.alert_queue_size = m_settings.alert_queue_size;
        psession->set_settings(settings);

        //ENable extentions
        psession->add_extension(&libtorrent::create_ut_metadata_plugin);
        psession->add_extension(&libtorrent::create_ut_pex_plugin);

        psession->start_dht();

        m_sessions.emplace_back(psession);
    }
}

InfoHash Crawler::handle_alert(libtorrent::alert* palert)
{
    InfoHash info_hash;

    switch (palert->type())
    {
    case libtorrent::dht_announce_alert::alert_type:
    {
        auto p2 = static_cast<libtorrent::dht_announce_alert*> (palert);

        //convert to hex digits
        std::stringstream ss;
        ss << p2->info_hash;

        info_hash = std::make_pair(ss.str(), InfoHashType::ANNOUNCE);
        break;
    }
    case libtorrent::dht_get_peers_alert::alert_type:
    {
        auto p3 = (libtorrent::dht_get_peers_alert*) palert;

        //convert to hex digits
        std::stringstream ss;
        ss << p3->info_hash;

        info_hash = std::make_pair(ss.str(), InfoHashType::GET_PEERS);
        break;
    }
    default:
        break;
    }

    return info_hash;
}

std::vector<InfoHash> Crawler::obtaining_info_hashes()
{
        LOGI("Obtaining infohashes.");

        std::vector<InfoHash> info_hashes;
        uint alert_count = 0;

        for (auto i{0u}; i < m_sessions.size(); ++i) {
            std::vector<libtorrent::alert*> alerts;

            m_sessions[i]->post_torrent_updates();
            m_sessions[i]->pop_alerts(&alerts);
            alert_count += alerts.size();

            for (auto* a : alerts) {
                auto info_hash = handle_alert(a);
                if(check(info_hash)) {
                    info_hashes.push_back(std::move(info_hash));
                }
                //TODO: delete a; ?
            }
        }
        LOGI("Total alerts spawned: {}", alert_count);
        LOGI("Total infohashes extracted: {}", info_hashes.size());

        return info_hashes;
}

std::size_t Crawler::on_downloading()
{
    return m_torrent_handles.size();
}

void Crawler::remove_invalid_torrents()
{
    auto count = m_torrent_handles.size();

    m_torrent_handles.erase(std::remove_if(m_torrent_handles.begin(), m_torrent_handles.end(), [](auto& tup) -> bool {
       return not std::get<0>(tup).is_valid() or
               std::get<0>(tup).is_paused();
    }), m_torrent_handles.end());

    count -= m_torrent_handles.size();
    if (count > 0) {
        LOGI("Remove invalid torrents. Total: {}", count);
    }
}

void Crawler::remove_expired() {
    auto count = m_torrent_handles.size();

    m_torrent_handles.erase(std::remove_if(m_torrent_handles.begin(), m_torrent_handles.end(), [&](auto& tup) -> bool {
       auto duration = std::chrono::system_clock::now() - std::get<1>(tup);
       return duration > std::chrono::seconds ( m_settings.max_download_duration );
    }), m_torrent_handles.end());

    count -= m_torrent_handles.size();
    if (count > 0) {
        LOGI("Remove expired torrents. Total: {}", count);
    }
}

void Crawler::remove_with_metadata() {
    auto count = m_torrent_handles.size();

    m_torrent_handles.erase(std::remove_if(m_torrent_handles.begin(), m_torrent_handles.end(), [](auto& tup) -> bool {
       return std::get<0>(tup).has_metadata();
    }), m_torrent_handles.end());

    count -= m_torrent_handles.size();
    if (count > 0) {
        LOGI("Remove torrents with metadata. Total: {}", count);
    }
}

bool Crawler::is_interesting(libtorrent::alert* a)
{
    return a->type() == libtorrent::dht_get_peers_alert::alert_type or
           a->type() == libtorrent::dht_announce_alert::alert_type;
}

std::vector<std::string> Crawler::extract_info_hashes(std::vector<libtorrent::alert*> alerts)
{
    std::vector<std::string> info_hashes;
    for (auto* a : alerts) {
        auto info_hash = handle_alert(a);
        if(check(info_hash)) {
            info_hashes.push_back(std::move(info_hash.first));
        }
    }
    return info_hashes;
}

void Crawler::add_to_download_list(std::vector<std::string>& info_hashes)
{
    LOGI("Add new torrents to download list");
    for (auto ih : info_hashes) {
        libtorrent::add_torrent_params params;

        //converting from hex to internal binary form
        std::stringstream ss(ih);
        ss >> params.info_hash;

        params.paused = false;
        params.save_path = "./";
        params.upload_mode = true;
        params.auto_managed = true;
        params.duplicate_is_error = true;


        try {
            auto handle = m_sessions[0]->add_torrent(params);
            handle.set_upload_limit(0);
            handle.set_download_limit(2048);

            auto t = std::make_tuple(handle, std::chrono::system_clock::now());
            m_torrent_handles.push_back(t);
        }
        catch(libtorrent::libtorrent_exception& e) {
            LOGE("Could not add torrent with info_hash {}. {}", ih, e.what());
        }
    }
    LOGI("Total add to download metadata : {}", info_hashes.size());
}

std::vector<libtorrent::alert*> Crawler::get_interesting_dht_messages()
{
    std::vector<libtorrent::alert*> msgs;
    uint alert_count = 0;

    for (auto i{0u}; i < m_sessions.size(); ++i) {
        std::vector<libtorrent::alert*> alerts;

        m_sessions[i]->post_torrent_updates();
        m_sessions[i]->pop_alerts(&alerts);
        alert_count += alerts.size();

        for (auto* a : alerts) {
            if(is_interesting(a)) {
                msgs.push_back(a);
            }
        }
    }

    LOGI("Total alerts spawned: {}", alert_count);
    LOGI("Total interesting extracted: {}", msgs.size());

    return msgs;
}

std::vector<TorrentMeta> Crawler::get_metadata()
{
    LOGI("Getting metadata");
    const std::string magnet_template = "magnet:?xt=urn:btih:";
    std::vector<TorrentMeta> torrents_metadata;

    bool metadata_downloaded = false;
    for (auto& tup : m_torrent_handles) {
        libtorrent::torrent_handle handle;
        std::chrono::time_point<std::chrono::system_clock> time_point;
        std::tie(handle, time_point) = tup;

        if ( handle.has_metadata() ) {
            LOGI("One of the torrents has metadata:");
            metadata_downloaded = true;

            auto t_info = handle.get_torrent_info();
            TorrentMeta torrent_metadata;

            torrent_metadata.name = t_info.name();
            LOGI("Name: {}", torrent_metadata.name);

            std::stringstream ss;
            ss << t_info.info_hash();
            torrent_metadata.info_hash = ss.str();
            LOGI("Magnet: {}", torrent_metadata.info_hash);

            auto file_storage = t_info.files();
            torrent_metadata.size = file_storage.total_size();
            LOGI("Size: {} bytes", torrent_metadata.size);

            LOGI("Files: ");
            for(auto i{0u}; i < file_storage.num_files(); i++ ) {
                FileMeta file_metadata;

                file_metadata.path = file_storage.file_path(i);
                LOGI("File path: {}", file_metadata.path);

                file_metadata.size = file_storage.file_size(i);
                LOGI("Size: {}", file_metadata.size);

                torrent_metadata.files.push_back(file_metadata);
            }
            torrents_metadata.push_back(torrent_metadata);
        }
    }

    if(metadata_downloaded == false) {
        LOGI("No one torrent has no metadata");
    }
    return torrents_metadata;
}

bool check(InfoHash& h) {
    return not h.first.empty();
}


