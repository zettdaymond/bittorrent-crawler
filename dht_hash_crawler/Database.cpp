#include "Database.h"

#include "Utils.h"
#include <sstream>

Database::Database(std::string name) : db(name.c_str())
{
    create_tables();
}

Database::~Database()
{
    LOGI("Destroing db");
}

void Database::create_tables()
{
    db.execute( "create table if not exists info_hashes( hash TEXT primary key)");

    db.execute( "create table if not exists info_hashes_announce("
                "    id integer primary key autoincrement,"
                "    hash TEXT,"
                "    address TEXT,"
                "    port INTEGER"
                "    hash_date DATETIME DEFAULT CURRENT_TIMESTAMP,"
                "    FOREIGN KEY(hash) REFERENCES info_hashes(hash)"
                ")"
                );
    db.execute( "create table if not exists info_hashes_get_peers("
                "    id integer primary key autoincrement,"
                "    hash TEXT,"
                "    hash_date DATETIME DEFAULT CURRENT_TIMESTAMP,"
                "    FOREIGN KEY(hash) REFERENCES info_hashes(hash)"
                ")"
                );

    db.execute( "create table if not exists files_metadata("
                "    id integer primary key autoincrement,"
                "    torrent_metadata_id integer,"
                "    file_path,"
                "    file_size integer,"
                "    FOREIGN KEY(id) REFERENCES torrents_metadata(id)"
                ");");

    db.execute( "create table if not exists torrents_metadata("
                "    id integer primary key autoincrement,"
                "    name,"
                "    hash,"
                "    total_size integer"
                ");" );

}

void Database::save_info_hashes(std::vector<std::string>& hashes)
{
    for (auto& hash : hashes) {
        sql::command ih_cmd(
                    db,
                    "INSERT INTO info_hashes (hash) VALUES (:val)"
                    );
        ih_cmd.bind(":val", hash, sql::copy);
        auto res = ih_cmd.execute();
        //LOGW("Res : {} ,after insert hash {}", res, hash);
    }
}

std::vector<std::string> Database::get_not_presented_info_hashes(const std::vector<std::string>& hashes)
{
    std::vector<std::string> result_hashes;
    for (auto& h : hashes) {
        sql::query q(db,
                     "SELECT hash FROM info_hashes WHERE hash == :val ");
        q.bind(":val", h, sql::copy);
        if ( q.begin() == q.end() ) {
            result_hashes.push_back(h);
        }
    }
    return result_hashes;
}

void Database::save_torrent_metadata(std::vector<TorrentMeta>& metadata)
{
    for (TorrentMeta& m: metadata) {
        sql::command tm_cmd(
                    db,
                    "insert into torrents_metadata (name, hash, total_size) VALUES (:name, :hash, :tsize)"
                    );
        sql::query q(db,
                     "SELECT id FROM torrents_metadata WHERE name == :name and hash == :hash and total_size == :tsize");

        q.bind(":name", m.name, sql::copy);
        q.bind(":hash", m.info_hash,sql::copy);
        q.bind(":tsize", std::to_string(m.size), sql::copy);

        tm_cmd.bind(":name", m.name, sql::copy);
        tm_cmd.bind(":hash", m.info_hash,sql::copy);
        tm_cmd.bind(":tsize", std::to_string(m.size), sql::copy);

        // if metadata doesn't exists, then add some
        auto is_metadata_inserted = false;
        if (q.begin() == q.end()) {
            tm_cmd.execute();
            is_metadata_inserted = true;
        }

        //if insert was be performed, add file metadata for this torrent
        if(is_metadata_inserted) {
            //use shadowing
            sql::query q(db,
                         "SELECT id FROM torrents_metadata WHERE name == :name and hash == :hash and total_size == :tsize");
            q.bind(":name", m.name, sql::copy);
            q.bind(":hash", m.info_hash,sql::copy);
            q.bind(":tsize", std::to_string(m.size), sql::copy);

            auto it = q.begin();

            if (it == q.end()) {
                LOGE("Could not find inserted torrent metadata. Maybe base was blocked?. Hash : {}, Torrent name : {}", m.info_hash, m.name);
                LOGE("Aquired metadata will be lost");
                continue;
            }

            int id = -1 ;
            (*it).getter() >> id;

            //insert corresponding files metadata
            for(auto& f : m.files) {
                sql::command insert_files(db,
                                          "INSERT INTO files_metadata (torrent_metadata_id, file_path, file_size) "
                                          "values (:id, :path, :size)");
                insert_files.bind(":id", std::to_string(id), sql::nocopy);
                insert_files.bind(":path", f.path, sql::copy);
                insert_files.bind(":size", std::to_string(f.size), sql::copy);

                auto res = insert_files.execute();
                if(res != 0 ) {
                    LOGE("Could not insert file entry. Result code: {}", res);
                }
            }
        }


    }
}

std::vector<std::string> Database::get_sorted_hashes(uint limit)
{
    //    sql::query q (db,
    //                  "SELECT info_hashes_announce.hash, count(info_hashes_announce.hash) as total "
    //                  "FROM info_hashes_announce "
    //                  "WHERE info_hashes_announce.hash NOT IN (SELECT hash FROM torrents_metadata) "
    //                  "GROUP BY info_hashes_announce.hash "
    //                  "ORDER BY total DESC "
    //                  "LIMIT :val "
    //                  );

    sql::query q (db,
                  "SELECT ih.hash, count(ih.hash) as total "
                  "FROM (select info_hashes_announce.hash from info_hashes_announce "
                  "      UNION ALL "
                  "      select info_hashes_get_peers.hash  from info_hashes_get_peers) as ih "
                  "WHERE ih.hash NOT IN (SELECT hash FROM torrents_metadata) "
                  "GROUP BY ih.hash "
                  "ORDER BY total DESC ");
    //q.bind(":val", std::to_string(limit), sql::nocopy);

    std::vector<std::string> out;
    for (auto v : q) {
        std::string info_hash;
        v.getter() >> info_hash;
        out.push_back(std::move(info_hash));
    }
    return out;
}

std::vector<std::string> Database::get_sorted_hashes(DHTMessage hash_aquire_method, uint limit)
{
    std::string query_template =
            "SELECT ih.hash, count(ih.hash) as total "
            "FROM __TEMPLATE_TABLE__ as ih "
            "WHERE ih.hash NOT IN (SELECT hash FROM torrents_metadata) "
            "GROUP BY ih.hash "
            "ORDER BY total DESC "
            "LIMIT :limit ";

    if (hash_aquire_method == DHTMessage::ANNOUNCE) {
        replace(query_template, "__TEMPLATE_TABLE__", "info_hashes_announce");
    }
    else if (hash_aquire_method == DHTMessage::GET_PEERS) {
        replace(query_template, "__TEMPLATE_TABLE__", "info_hashes_get_peers");
    }
    else {
        assert(false && "DHTMessage is invalid");
    }

    sql::query q (db,query_template.c_str());
    q.bind(":limit", std::to_string(limit), sql::nocopy);

    std::vector<std::string> out;
    for (auto v : q) {
        std::string info_hash;
        v.getter() >> info_hash;
        out.push_back(std::move(info_hash));
    }
    return out;
}


void Database::save_dht_messages(std::vector<Message> messages)
{
    int announce_count = 0;
    int get_peers_count = 0;
    for (auto& m : messages) {
        sql::command iha_cmd(
                    db,
                    "INSERT INTO `info_hashes_announce`(`hash`, `address`, `port`) "
                    "VALUES (:hash, :address, :port);"
                    );
        sql::command ihgp_cmd(
                    db,
                    "INSERT INTO `info_hashes_get_peers`(`hash`) VALUES (:val);"
                    );

        if(m.info_hash.empty()) {
            continue;
        }
        switch (m.type) {

        case Message::Type::ANNOUNCE:

            iha_cmd.bind(":hash", m.info_hash, sqlite3pp::copy);
            iha_cmd.bind(":address", m.ip, sqlite3pp::copy);
            iha_cmd.bind(":port", std::to_string(m.port), sqlite3pp::copy);
            iha_cmd.execute();

            announce_count++;
            break;

        case Message::Type::GET_PEERS:

            ihgp_cmd.bind(":val", m.info_hash, sqlite3pp::copy);
            ihgp_cmd.execute();
            get_peers_count++;
            break;

        default:
            assert(false && "This is impossible");
            break;
        }
    }
    if(announce_count > 0 || get_peers_count > 0) {
        LOGI("Messages saved to db. get_peers : {}, announce : {} ", get_peers_count, announce_count);
    }
}
