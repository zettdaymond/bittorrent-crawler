#pragma once

#include "thirdparty/spdlog/spdlog.h"

#include "Types.h"

#define LOGI(message, args...) \
    do { \
        spdlog::get("default")->info( message, ##args ); \
    } while (false);

#define LOGW(message, args...) \
    do { \
        spdlog::get("default")->warn( message, ##args ); \
    } while (false);

#define LOGE(message, args...) \
    do { \
        spdlog::get("default")->error( message, ##args ); \
    } while (false);

#define LOGD(message, args...) \
    do { \
        spdlog::get("default")->debug( message, ##args ); \
    } while (false);


void init_log(Settings::LOG_LEVEL level);

void init_log(std::string path, Settings::LOG_LEVEL level);

std::vector<std::string> join_same_hashes(const std::vector<std::__cxx11::string>& hashes);

bool replace(std::string& str, const std::string& from, const std::string& to);
