#pragma once

#include <string>
#include <vector>

struct FileMeta
{
    std::string path;
    int size;
};

struct TorrentMeta
{
    std::string name;
    std::string info_hash;
    std::size_t size;
    std::vector<FileMeta> files;
};

struct Message
{
    enum class Type {
        GET_PEERS,
        ANNOUNCE
    };

    std::string info_hash;
    std::string ip;
    int port;
    Type type;
};

struct Settings
{
    enum class LOG_LEVEL {
        DUBUG,
        INFO,
        WARNING,
        ERROR
    };

    int time_per_iteration = 30;
    std::string database_path = "./crawler.db";
    LOG_LEVEL log_level = LOG_LEVEL::WARNING;
    std::string log_path = "";
    std::string crawler_session_state = "./crawler.state";
    bool use_get_peers_hashes = true;
    std::string fetcher_session_state = "./crawler.state";
    int download_queue_size = 30;
    int torrent_expiration_time = 60;
    std::string save_file_path = "./";
};
