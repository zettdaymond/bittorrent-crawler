#include <csignal>
#include <chrono>

#include "thirdparty/cpptoml.h"

#include "DhtCrawler.h"
#include "Fetcher.h"
#include "Utils.h"

using TOMLConfig = std::shared_ptr<cpptoml::table>;

using Clock = std::chrono::system_clock;
using TimePoint = std::chrono::time_point<Clock>;
using Seconds = std::chrono::seconds;

bool main_loop_exit = false;

void signal_handler( int signum )
{
    switch (signum)
    {
        case SIGTERM:
            LOGI("Recieve SIGTERM os signal.");
        break;
        case SIGABRT:
            LOGI("Recieve SIGABRT os signal.");
        break;
        case SIGINT:
            LOGI("Recieve SIGINT os signal.");
        break;
    }
    LOGI("Terminating the program.");
    main_loop_exit = true;
}

Settings configure(TOMLConfig toml_config)
{
    Settings out;
    out.time_per_iteration = toml_config->get_as<int64_t>("time_per_iteration").value_or(out.time_per_iteration);
    out.database_path = toml_config->get_as<std::string>("database_path").value_or(out.database_path);
    auto log_level = toml_config->get_as<std::string>("log_level");
    if(log_level) {
        if(*log_level == "debug") {
            out.log_level = Settings::LOG_LEVEL::DUBUG;
        }
        if(*log_level == "info") {
            out.log_level = Settings::LOG_LEVEL::INFO;
        }
        if(*log_level == "warning") {
            out.log_level = Settings::LOG_LEVEL::WARNING;
        }
        if(*log_level == "error") {
            out.log_level = Settings::LOG_LEVEL::ERROR;
        }
        //else use default
    }
    out.log_path = toml_config->get_as<std::string>("log_path").value_or(out.log_path);

    out.crawler_session_state = toml_config->get_qualified_as<std::string>("crawler.session_state").value_or(out.crawler_session_state);
    out.use_get_peers_hashes = toml_config->get_as<bool>("crawler.use_get_peers_hashes").value_or(out.use_get_peers_hashes);

    out.fetcher_session_state = toml_config->get_qualified_as<std::string>("fetcher.session_state").value_or(out.fetcher_session_state);
    out.download_queue_size = toml_config->get_qualified_as<int64_t>("fetcher.download_queue_size").value_or(out.download_queue_size);
    out.torrent_expiration_time = toml_config->get_qualified_as<int64_t>("fetcher.torrent_expiration_time").value_or(out.torrent_expiration_time);
    out.save_file_path = toml_config->get_qualified_as<std::string>("fetcher.save_file_path").value_or(out.save_file_path);

    return out;
}

int main(int argc, char *argv[])
{
    Settings settings;

    //Adjust settings if config provided
    if (argc == 2) {
        TOMLConfig config;
        try {
            auto toml_config = cpptoml::parse_file(argv[1]);
            settings = configure(toml_config);
        }
        catch (std::exception& e){
            std::cerr << "Could not parse file: " << argv[1] << std::endl;
            std::cerr << e.what() << std::endl;
            std::terminate();
        }
    }

    //Init loger
    if(not settings.log_path.empty()) {
        try {
            init_log(settings.log_path, settings.log_level);
        }
        catch (std::exception& e){
            std::cerr << "Error on log initialization " << e.what() << std::endl;
            std::terminate();
        }
    } else {
        init_log(settings.log_level);
    }

    //Setup signal handlers
    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);
    std::signal(SIGABRT, signal_handler);

    //Start main loop
    {
        int time_per_iteration = settings.time_per_iteration;

        LOGI("Init databases");
        Database db(settings.database_path);

        LOGI("Creating dht crawler.");
        CrawlerSettings cs;
        cs.use_get_peers_hashes = settings.use_get_peers_hashes;
        DhtCrawler crawler( db, cs);

        LOGI("Creating metadata fetcher");
        FetcherSettings fs;
        fs.queue_size = settings.download_queue_size;
        fs.timeout =  settings.torrent_expiration_time;
        fs.save_path = settings.save_file_path;
        Fetcher fetcher(db, fs);

        LOGI("Loading previous dht state");
        try {
            crawler.load_state(settings.crawler_session_state);
            fetcher.load_state(settings.fetcher_session_state);
        }
        catch(std::exception& e) {
            LOGE("Could not load crawler/fetcher session state from disk. {}", e.what());
            LOGE("Default will be used ")
        }

        while(not main_loop_exit) {
            try {
                auto start_time = Clock::now();

                crawler.step();
                fetcher.step();

                auto duration = Clock::now() - start_time;
                auto remain = std::chrono::duration_cast<Seconds>(Seconds(time_per_iteration) - duration);

                if (remain.count()) {
                    sleep(remain.count());
                }
            }
            catch(std::exception& e) {
                LOGE("Program raise an exeption : {}", e.what());
            }
        }

        LOGI("Saving dht nodes before exit");
        try {
            crawler.save_state(settings.crawler_session_state);
            fetcher.save_state(settings.fetcher_session_state);
        }
        catch(std::exception& e) {
            LOGE("Could not save crawler/fetcher session state on disk. {}", e.what());
        }

        LOGI("Shutdown.");

        //workaround over libtorrent ~session() problem
        std::exit(0);
    }

    return 0;
}
