#include "Fetcher.h"
#include <fstream>

#include <libtorrent/session_handle.hpp>
#include <libtorrent/settings_pack.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/torrent_info.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/bencode.hpp>
#include <libtorrent/bdecode.hpp>

#include "Utils.h"
#include "Exception.h"

Fetcher::Fetcher(Database& db) : m_db(db)
{
    m_session = SessionPtr(new libtorrent::session());

    m_session->set_upload_rate_limit(0);
    m_session->set_download_rate_limit(2048 * m_settings.queue_size);

    m_session->add_dht_router(std::make_pair("router.bittorrent.com", 6881));
    m_session->add_dht_router(std::make_pair("router.utorrent.com", 6881));
    //m_session->add_dht_router(std::make_pair("router.bitcomet.com", 6881));
    //m_session->add_dht_router(std::make_pair("dht.transmissionbt.com", 6881));

    m_session->start_dht();

    lt::add_torrent_params params;
    params.paused = false;
    params.save_path = m_settings.save_path;
    params.storage_mode = lt::storage_mode_sparse;
    params.upload_mode = true;
    params.auto_managed = false;
    params.duplicate_is_error = true;

    m_settings.torrent_add_params = params;
}

Fetcher::Fetcher(Database& db, FetcherSettings s) : Fetcher(db)
{
    m_settings = s;
}

Fetcher::~Fetcher()
{
    for(auto& tup : m_torrent_handles) {
        std::get<0>(tup).pause();
    }
    m_torrent_handles.clear();
    LOGI("Fetcher destroyed")
}

void Fetcher::save_state(std::string filename)
{
    lt::entry e;
    m_session->save_state(e);

    std::vector<char> out_buf;
    lt::bencode(std::back_inserter(out_buf), e);

    std::ofstream out_stream(filename,std::ios::trunc);
    out_stream.write(&out_buf[0], out_buf.size());
    out_stream.close();
}

void Fetcher::load_state(std::string filename)
{
    std::ifstream in_stream(filename);
    std::vector<char> buf((std::istreambuf_iterator<char>(in_stream)),
                          std::istreambuf_iterator<char>());

    lt::bdecode_node dn;
    lt::error_code ec;
    lt::bdecode(&buf[0], &buf[0] + buf.size(), dn, ec);

    if(ec) {
        throw StrException(ec.message());
    }

    m_session->load_state(dn);
}

void Fetcher::add_to_dl(const std::vector<std::string>& info_hashes)
{
    for (auto& ih : info_hashes) {
        if(m_torrent_handles.size() == m_settings.queue_size) {
            break;
        }

        lt::add_torrent_params params = m_settings.torrent_add_params;

        try {
            std::string magnet = "magnet:?xt=urn:btih:" + ih;
            auto handle = lt::add_magnet_uri(*m_session, magnet, m_settings.torrent_add_params );
            handle.set_upload_limit(0);
            handle.set_download_limit(m_settings.download_rate_per_torrent);

            auto t = std::make_tuple(handle, std::chrono::system_clock::now());
            m_torrent_handles.push_back(t);
        }
        catch(libtorrent::libtorrent_exception& e) {
            LOGE("Could not add torrent with info_hash {}. {}", ih, e.what());
        }
    }
}

void Fetcher::step()
{
    //pop dht messages queue
    std::vector<libtorrent::alert*> alerts;
    m_session->post_torrent_updates();
    m_session->pop_alerts(&alerts);

    //get new info_hashes without downloaded metadata.
    //sorted by count of peers (retriewed by announce messages)
    //limit count by empty space in queue
    //get 80% of most popular announce.
    auto announce_count = m_settings.queue_size * 0.8;
    auto announce_hashes = m_db.get_sorted_hashes(Database::DHTMessage::ANNOUNCE, announce_count);

    //remove already presented in queue
    remove_queued_hashes(announce_hashes);
    LOGD("Total announce hashes to aquire metadata: {}", announce_hashes.size());

    //fill last space in download queue (ideally 20%) by get_peers requests.
    auto get_peers_count = m_settings.queue_size /*- announce_hashes.size() */;
    auto get_peers_hashes = m_db.get_sorted_hashes(Database::DHTMessage::GET_PEERS, get_peers_count);

    //remove already presented in queue
    remove_queued_hashes(get_peers_hashes);
    LOGD("Total get_peers hashes to aquire metadata: {}", get_peers_hashes.size());

    //if announce_hashes is too few, then request more get_peers_hashes
    //if(announce_hashes.size() < announce_count) {
    //   get_peers_hashes = m_db.get_sorted_hashes(Database::DHTMessage::GET_PEERS, get_peers_count + announce_count - announce_hashes.size());
    //}

    //merge hashes to one container. It starts with announce hashes, and ends witn get_peers.
    //his size might be more than queue_size, up to queue_size * 2. But only a few of represented hashes will be added.
    decltype(announce_hashes) hashes = std::move(announce_hashes);
    std::move(get_peers_hashes.begin(), get_peers_hashes.end(), std::back_inserter(hashes));

    //remove duplicated
    hashes = join_same_hashes(hashes);

    //remove already presented in queue
    remove_queued_hashes(hashes);

    //TODO: remove paused?
    LOGD("Total hashes to aquire metadata: {}", hashes.size());

    //scedule new torrents to download list
    if(m_torrent_handles.size() < m_settings.queue_size) {
        add_to_dl(hashes);
    }

    //Obtain metadata
    auto metadata = get_metadata();
    if(not metadata.empty()) {
        LOGW("Metadata aquired. Total: {} Saving to db.", metadata.size());
        m_db.save_torrent_metadata(metadata);
    }

    //Remove queued torrents if they have downloaded metadata or timed out.
    auto timeout_count = 0;
    auto paused_count = 0;

    m_torrent_handles.erase(std::remove_if(m_torrent_handles.begin(), m_torrent_handles.end(),
    [&](auto& tup) -> bool {
        lt::torrent_handle handle;
        std::chrono::time_point<std::chrono::system_clock> start_time;
        std::tie(handle, start_time) = tup;

        auto duration = std::chrono::system_clock::now() - start_time;
        if ( duration > std::chrono::seconds ( m_settings.timeout ) ) {
            handle.pause();
            m_session->remove_torrent(handle);
            timeout_count += 1;
            return true;
        }
        if (handle.is_paused()) {
            paused_count += 1;
            return true;
        }
    }), m_torrent_handles.end());

    LOGI("Total torrets in download queue: {}", m_torrent_handles.size())

    if (timeout_count > 0) {
        LOGD("Remove time out torrents. Total: {}", timeout_count);
    }

    if(paused_count > 0) {
        LOGD("Remove paused torrents. Total: {}", paused_count);
    }

    lt::entry e = m_session->dht_state();
    LOGD("Metadata Fetcher dht nodes count {}", e["nodes"].list().size());
}

std::vector<TorrentMeta> Fetcher::get_metadata()
{
    const std::string magnet_template = "magnet:?xt=urn:btih:";
    std::vector<TorrentMeta> torrents_metadata;

    uint i = 0;
    for (auto& tup : m_torrent_handles) {
        lt::torrent_handle handle;
        std::chrono::time_point<std::chrono::system_clock> time_point;
        std::tie(handle, time_point) = tup;

        if ( handle.has_metadata() ) {

            auto t_info = handle.get_torrent_info();
            TorrentMeta torrent_metadata;

            torrent_metadata.name = t_info.name();

            std::stringstream ss;
            ss << t_info.info_hash();

            torrent_metadata.info_hash = ss.str();

            auto file_storage = t_info.files();
            torrent_metadata.size = file_storage.total_size();

            for(auto i{0u}; i < file_storage.num_files(); i++ ) {
                FileMeta file_metadata;
                file_metadata.path = file_storage.file_path(i);
                file_metadata.size = file_storage.file_size(i);
                torrent_metadata.files.push_back(file_metadata);
            }
            torrents_metadata.push_back(torrent_metadata);

            //stop torrent. it will be removed later
            handle.pause();

            LOGI("One of the torrents with hash: {} has metadata. His name is {}", ss.str(), t_info.name());
        }

        i++;
    }
    return torrents_metadata;
}

void Fetcher::remove_queued_hashes(std::vector<std::string>& hashes)
{
    for(auto& dl : m_torrent_handles) {
        if(hashes.empty()) {
            break;
        }
        std::stringstream ss;
        ss << std::get<0>(dl).info_hash();

        hashes.erase(std::remove_if(hashes.begin(), hashes.end(), [&] (auto& h) -> bool {
            return h == ss.str();
        }));
    }
    LOGD("Total hashes without already downloading: {}", hashes.size());
}
